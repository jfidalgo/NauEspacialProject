package cat.xtec.ioc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.objects.ScrollHandler;
import cat.xtec.ioc.objects.Spacecraft;
import cat.xtec.ioc.utils.Settings;

public class DifficultyScreen extends ChangeListener implements Screen {


    private SpaceRace game;

    String dificultat;

    // Objectes necessaris
    private Stage stage;
    private Spacecraft spacecraft;
    private ScrollHandler scrollHandler;

    // Encarregats de dibuixar elements per pantalla
    private ShapeRenderer shapeRenderer;
    private Batch batch;


    //Creacio dels botons per pausar el joc
    TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();


    TextButton facil;
    TextButton medio;
    TextButton dificil;

    public DifficultyScreen(SpaceRace game) {

        this.game = game;


        // Creem la càmera de les dimensions del joc
        OrthographicCamera camera = new OrthographicCamera(Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
        // Posant el paràmetre a true configurem la càmera per a
        // que faci servir el sistema de coordenades Y-Down
        camera.setToOrtho(true);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(Settings.GAME_WIDTH, Settings.GAME_HEIGHT, camera);

        // Creem l'stage i assginem el viewport
        stage = new Stage(viewport);

        // Afegim el fons
        stage.addActor(new Image(AssetManager.background));

        // Iniciem la música
        AssetManager.music.play();

        // Creem el ShapeRenderer
        // shapeRenderer = new ShapeRenderer();


        // A l'iniciar el joc carreguem els recursos
        AssetManager.load();

        // batch = stage.getBatch();

        //scrollHandler = new ScrollHandler(this);


        Gdx.input.setInputProcessor(stage);

        //------------------

        TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();
        estilo.font = AssetManager.font;


        //-----------------


        facil = new TextButton("Facil", estilo);
        facil.setName("Facil");
        facil.getLabel().setName("Facil");
        facil.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT * 0.15f);
        facil.addListener(this);



        //-----------------

        medio = new TextButton("Media", estilo);
        medio.setName("Media");
        medio.getLabel().setName("Media");
        medio.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT * 0.45f);
        medio.addListener(this);


        //-----------------------


        dificil = new TextButton("Dificil", estilo);
        dificil.setName("Dificil");
        dificil.getLabel().setName("Dificil");
        dificil.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT * 0.75f);
        dificil.addListener(this);

        //--------------------------


        stage.addActor(facil);
        stage.addActor(medio);
        stage.addActor(dificil);


        //-----------------
    }


    private void anadirTextButtons() {
        TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();
        estilo.font = AssetManager.font;

        facil = new TextButton("Facil", estilo);
        //facil.getLabel().setName("Facil");
        facil.setName("Facil");
        // facil.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT * 0.15f);
        facil.addListener(this);

        medio = new TextButton("Media", estilo);
        medio.setName("Media");
        //  medio.getLabel().setName("Media");
        medio.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT * 0.45f);
        medio.addListener(this);

        dificil = new TextButton("Dificil", estilo);
        dificil.setName("Dificil");
        // dificil.getLabel().setName("Dificil");
        dificil.setPosition(Settings.GAME_WIDTH / 2, Settings.GAME_HEIGHT * 0.75f);
        dificil.addListener(this);


        stage.addActor(facil);
        stage.addActor(medio);
        stage.addActor(dificil);

        System.out.println("Constructor");

        //game.setScreen(new SplashScreen(game , "Media"));


    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        stage.draw();
        stage.act(delta);

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void changed(ChangeEvent event, Actor actor) {
        System.out.println("changed");
        if (actor.getName().equalsIgnoreCase("Facil")) {
            dificultat = "facil";
            game.setScreen(new SplashScreen(game, dificultat));
            dispose();
        } else if (actor.getName().equalsIgnoreCase("Media")) {
            dificultat = "media";
            game.setScreen(new SplashScreen(game, dificultat));
            dispose();
        } else if (actor.getName().equalsIgnoreCase("Dificil")) {
            dificultat = "dificil";
            game.setScreen(new SplashScreen(game, dificultat));
            dispose();
        }


    }
}
